class Subscriber < AWS::Record::Base
  string_attr :h
  string_attr :email
  string_attr :message
  datetime_attr :uploaded_at
  datetime_attr :unsubscribed_at
  timestamps

  def unsubscribed?
    self.unsubscribed_at ? true : false
  end

  def self.find_by_h(h)
    where(:h => h).first
  end 

  def self.subscribe(h, mess)
    subscriber = find_by_h(h)

    return nil unless subscriber

    if subscriber.unsubscribed?
      subscriber.update_attributes(:unsubscribed_at => nil)
    else
      subscriber.update_attributes(:unsubscribed_at => Time.now, :message => mess)
    end    

    subscriber
  end

end    
