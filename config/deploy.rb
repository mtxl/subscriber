require 'rvm/capistrano'
require 'bundler/capistrano'
set :rvm_ruby_string, '2.1.6@subscriber'
set :rvm_type, :system
set :rvm_bin_path, "/usr/local/rvm/bin"

#main
set :application, "subscriber"
server 'mirdo.ru', :app, :web, :db, :primary => true

#server
set :deploy_to, "/srv/www/subscriber"
default_run_options[:pty] = true
ssh_options[:forward_agent] = true
set :user, "subscriber"
set :use_sudo, false

#repo
set :scm, :git
set :repository,  "git@bitbucket.org:mtxl/subscriber"
set :branch, "master"
#set :deploy_via, :remote_cache

namespace :deploy do

  task :start, :roles => :app do
     run "touch #{current_path}/tmp/restart.txt"
  end

  task :stop, :roles => :app do
   # Do nothing.
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

end  

#before "deploy:setup", "rvmrc:create", "aws:configure"
after "deploy:setup", "rvmrc:create", "aws:configure"

after "deploy:update_code", "rvmrc:symlink", "aws:symlink"

namespace :aws do
  desc "Create aws yaml in shared path"
  task :configure do
    set :aws_password do
      Capistrano::CLI.password_prompt "AWS passwotd"
    end
    aws_config = <<-EOS
access_key_id: AKIAJIML3UNBQXGWKBKA   
secret_access_key: #{aws_password}  # Password
    EOS

    run "mkdir -p #{shared_path}/config"
    put aws_config, "#{shared_path}/config/aws.yml"
  end

  desc "Make symlink for aws yaml"
  task :symlink do
    run "ln -nfs #{shared_path}/config/aws.yml #{latest_release}/config/aws.yml"
  end
end  

namespace :rvmrc do
  desc "Create .rvmrc"
  task :create do
    put "rvm 2.1.6@subscriber", "#{shared_path}/.rvmrc"
  end
  desc "Make symlink for .rvmrc"
  task :symlink do
    run "ln -nfs #{shared_path}/.rvmrc #{latest_release}/.rvmrc"
  end
end


