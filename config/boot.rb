ENV["RACK_ENV"] ||= "development"

require 'bundler'

Bundler.require(:default, ENV["RACK_ENV"].to_sym)

Dir["./lib/**/*.rb"].each { |f| require f }

aws_config_path = File.expand_path(File.dirname(__FILE__)+"/aws.yml")
AWS.config(YAML.load(File.read(aws_config_path)))
AWS::Record.domain_prefix = ENV["RACK_ENV"]


