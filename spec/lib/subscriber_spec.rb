require 'spec_helper'
require 'spec/mocks'

describe Subscriber do

  let(:h) { Digest::SHA1.new.to_s }

  let(:unsubscribed_at) { Time.now }

  let(:subscriber) { Subscriber.new(:unsubscribed_at => unsubscribed_at) }

  before :all do
    stub_config
  end

  context "#initilize" do

    it "should be valid class" do
      subscriber.should be_a_kind_of (AWS::Record::Base)
    end  

  end

  context '#unsubscribed?' do

    
    it "should be true " do
      subscriber.unsubscribed?.should == true
    end

    context "when not unsubscribed" do

      let(:unsubscribed_at) { nil }

      it "should be false " do
        subscriber.unsubscribed?.should == false
      end

    end

  end     

  context :find_by_h do
    it "should return nil" do
      Subscriber.should_receive(:where).with(:h => h).and_return([])
      Subscriber.find_by_h(h).should == nil
    end  

    it "should return subscriber" do
      Subscriber.should_receive(:where).with(:h => h).and_return([subscriber])
      Subscriber.find_by_h(h).should == subscriber
    end  

  end

  context :subscribe do

    let(:mess) { "some pretty mess" }
  
    context "when subscriber exist and unsubscribed" do
      
      it "should be subscribed" do
        Subscriber.should_receive(:where).with(:h => h).and_return([subscriber])
        Subscriber.subscribe(h, mess).should == subscriber
        subscriber.unsubscribed?.should == false
      end

    end

    context "when subscriber exist and not unsubscribed" do

      let(:unsubscribed_at) { nil }       

      it "should be subscribed" do
        Subscriber.should_receive(:where).with(:h => h).and_return([subscriber])
        Subscriber.subscribe(h, mess).should == subscriber
        subscriber.unsubscribed?.should == true
        subscriber.message.should == mess
      end

    end

    context "when subscrinber not exist" do
      it "should return nil" do
        Subscriber.should_receive(:where).with(:h => h).and_return([])
        Subscriber.subscribe(h, mess).should == nil
      end  
    end

  end

end
    
