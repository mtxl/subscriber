ENV['RACK_ENV'] = "test"
require File.expand_path(File.dirname(__FILE__) + "/../config/boot")

Webrat.configure do |config|
  config.mode = :rack
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.include Webrat::Methods
  config.include Webrat::Matchers
end

require File.join(File.dirname(__FILE__), '../subscriber')

def stub_config
  AWS.config(
    :stub_requests => true,
    :access_key_id => 'ACCESS_KEY_ID',
    :secret_access_key => 'SECRET_ACCESS_KEY')
end

