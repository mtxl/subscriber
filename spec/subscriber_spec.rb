require 'spec_helper'

describe "Subscriber" do

  let(:h) { Digest::SHA1.new.to_s }

  let(:unsubscribed_at) { Time.now }

  let(:subscriber) { Subscriber.new(:unsubscribed_at => unsubscribed_at) }

  before :all do
    stub_config
  end

  def app
    Sinatra::Application.new
  end
  
  context "GET '/unsubscribe/:hexdigest'" do

    it "should be successful" do
      Subscriber.should_receive(:where).with(:h => h).and_return([subscriber])
      get "/unsubscribe/#{h}"
      last_response.should be_ok
    end

    it "should not be successful" do
      Subscriber.should_receive(:where).with(:h => h).and_return([])
      get "/unsubscribe/#{h}"
      last_response.should be_ok
    end

  end

end
