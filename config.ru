require File.join(File.dirname(__FILE__), 'subscriber')

set :run, false
set :environment, :production

run Sinatra::Application
