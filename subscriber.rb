# encoding: utf-8
require File.dirname(__FILE__) + '/config/boot.rb'
require 'sinatra'
require 'logger'

logger = Logger.new("#{settings.root}/log/#{settings.environment}.log")

get '/unsubscribe/:h' do |h|
  @subscriber = Subscriber.find_by_h(h)
  if @subscriber 
    email = @subscriber.email
    logger.error("hexdisgest #{h} don't have email") unless email
    haml :unsubscribe
  else
    @message = "ошибочный код #{h}"
    logger.error("hexdisgest #{h} doesn't exist") 
    haml :message
  end    
end

post '/unsubscribe/:h' do |h|
  message = params[:message]
  @subscriber = Subscriber.subscribe(h, message) 
  if @subscriber 
    haml :unsubscribe 
  else 
    @message = "ошибочный код #{h}"
    haml :message
  end  

end    

not_found do
  @message = "Запрошенная страница не существует"
  haml :message
end

error do
    'Sorry there was a nasty error - ' + env['sinatra.error'].name
end
